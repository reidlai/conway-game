const chai = require('chai')
const assertArrays = require('chai-arrays')
const sinon = require('sinon')

chai.use(assertArrays)

describe('Cell tests', () => {
  describe('Any live cell with fewer than two live neighbours dies', () => {

    it('live cell should die without neighbours in next tick')

    it('live cell should die with only one neighbours')

  })

  describe('Any live cell with two or three live neighbours lives on to the next generation', () => {

    it('live cell should survive with two neighbours in next tick')

    it('live cell should survive with three neighbours in next tick')

  })

  describe('Any live cell with more than three live neighbours dies', () => {

    it('live cell should die with more than 3 neighbours in next tick')

  })

  describe('Any dead cell with exactly three live neighbours becomes a live cell', () => {

    it('died cell should become live cell with 3 live neighbours in next tick')
    
  })
})
