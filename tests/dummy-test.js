const assert = require('chai').assert

describe('Dummy Test Cases', () => {
  describe('Test expect true', () => {
    it('Expect and actual result is true', () => {
      assert.isTrue(true, 'return value is false')
    })
  })

  describe('Test expect false', () => {
    it('Expect and actual result is false', () => {
      assert.isFalse(false, 'return value is true')
    })
  })
})
