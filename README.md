# Conway's Game of Life

## Linting

Linting has been implemented by using ESLint.  Developer can run the following
command to start linting at application root directory:

```
npm run lint
```
